/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import 'styles/app.scss';

import '@popperjs/core';
// import * as bootstrap from 'bootstrap';
import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from '@/App.vue';
import router from '@/router';
import { registerLayouts } from '@/layouts/register';
import vuetify from '@/plugins/vuetify';

export const process = {
    env: {
        APP_NAME: 'PretItem',
    },
};

const app = createApp(App);
app.use(createPinia());
app.use(router);
// app.use(bootstrap);
app.use(vuetify);
app.mount('#app');

registerLayouts(app);

// Directives
// app.directive('tooltip', {
//     mounted(el, binding) {
//         const tooltip = new bootstrap.Tooltip(el, {
//             placement: binding.arg,
//             title: binding.value,
//             trigger: 'hover',
//         });
//     },
// });
// app.directive('popover', {
//     mounted(el) {
//         const popover = new bootstrap.Popover(el);
//     },
// });
