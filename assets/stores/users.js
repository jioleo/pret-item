import { defineStore, storeToRefs } from 'pinia';
import { ref } from 'vue';
import client from '@/stores/services/api/index';
import { useLoginStore } from '@/stores/auth';
import { useAlertStore } from '@/stores/alert';

const baseUrl = '/users';

export const useUsersStore = defineStore('users', () => {
    const { error: alertError } = useAlertStore();
    const users = ref({});
    const user = ref({});

    async function register(userObj) {
        try {
            await client.post('/register', userObj);
        } catch (error) {
            alertError(error);
        }
    }
    async function getAll() {
        users.value = { loading: true };
        try {
            users.value = await client.get(baseUrl);
        } catch (error) {
            alertError(error);
        }
    }
    async function getById(iri) {
        user.value = { loading: true };
        try {
            user.value = await client.get(iri);
        } catch (error) {
            alertError(error);
        }
    }
    async function update(id, params) {
        await client.put(`${baseUrl}/${id}`, params);

        // update stored user if the logged in user updated their own record
        const loginStore = useLoginStore();
        if (id === loginStore.user['@id']) {
            const store = storeToRefs(loginStore);
            // update local storage
            const currentUser = { ...store.user, ...params };
            localStorage.setItem('user', JSON.stringify(currentUser));

            // update auth user in pinia state
            loginStore.user = currentUser;
        }
    }
    async function remove(iri) {
        // add isDeleting prop to user being deleted
        users.value.find((x) => x['@id'] === iri).isDeleting = true;

        await client.delete({ url: iri, baseUrl: '' });

        // remove user from list after deleted
        users.value = users.value.filter((x) => x['@id'] !== iri);

        // auto logout if the logged in user deleted their own record
        const loginStore = useLoginStore();
        if (iri === loginStore.user['@id']) {
            loginStore.logout();
        }
    }

    return {
        register, getAll, getById, update, remove,
    };
});
