import client from '@/stores/services/api';

const baseUrl = '/tags';

export default {
    getCollection(params) {
        return client.get(`${baseUrl}`, { params });
    },

    add(tag) {
        return client.post(baseUrl, tag);
    },

    getOneBySlug(slug) {
        return client.get(`${baseUrl}/${slug}`);
    },

    edit(tag) {
        return client.patch(`${baseUrl}/${tag.slug}`, tag);
    },

    delete(slug) {
        return client.delete(`${baseUrl}/${slug}`);
    },
};
