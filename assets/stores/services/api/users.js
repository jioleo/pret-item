import client from '@/stores/services/api';

const baseUrl = '/users';

export default {
    getByPage(page) {
        return client.get(baseUrl, { page });
    },

    getOneByIri(iri) {
        return client({
            url: iri,
            baseURL: '',
            method: 'GET',
        });
    },

    add(user) {
        return client.post(baseUrl, user);
    },

    edit(iri, user) {
        return client.patch({ url: iri, baseURL: '' }, user);
    },

    delete(iri) {
        return client.delete({ url: iri, baseURL: '' });
    },
};
