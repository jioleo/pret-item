import { useLoginStore } from '@/stores/auth';

const baseURL = '/api';

const client = axios.create({
    baseURL,
    headers: {
        'Content-type': 'application/json',
    },
});

function setAuthorization(config, token) {
    config.headers.Authorization = `Bearer ${token}`;
}

client.interceptors.request.use(
    (config) => {
        if (config.method === 'patch') {
            config.headers['Content-Type'] = 'application/merge-patch+json';
        }
        const { user } = useLoginStore();
        const isLoggedIn = !!user?.token;

        if (isLoggedIn) {
            setAuthorization(config, user.token);
        } else {
            delete client.defaults.headers.common.Authorization;
        }
        return config;
    },

    (error) => Promise.reject(error),
);

client.interceptors.response.use(
    (response) => response,
    async (error) => {
        const { config, message } = error;
        if (!config || message.includes('timeout') || message.includes('Network Error')) {
            return Promise.reject(error);
        }
        const loginStore = useLoginStore();
        const { user } = storeToRefs(loginStore);
        const { refreshToken, logout } = loginStore;

        if ([401].includes(error.response.status) && user.value && !config.retry) {
            config.retry = true;
            await refreshToken();
            return client(config);
        }
        if (config.retry) {
            logout();
            return Promise.reject(Error('Failed to refresh token'));
        }
        return Promise.reject(error);
    },
);

export default client;
