import client from '@/stores/services/api';

export default {
    login(email, password) {
        return client.post('/login', { email, password });
    },
    refresh(refreshToken) {
        return client({
            url: '/token/refresh',
            baseURL: '',
            method: 'POST',
            data: { refresh_token: refreshToken },
        });
    },
};
