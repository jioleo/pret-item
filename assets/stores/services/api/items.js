import client from '@/stores/services/api';

const baseUrl = '/items';

export default {
    getCollection(params) {
        return client.get(`${baseUrl}`, { params });
    },

    getCatalog(params) {
        return client({
            url: '/api/catalog',
            baseURL: '',
            method: 'GET',
            params,
        });
    },

    add(item) {
        return client.post(baseUrl, item);
    },

    getOneBySlug(slug) {
        return client.get(`${baseUrl}/${slug}`);
    },

    edit(item) {
        return client.patch(`${baseUrl}/${item.slug}`, item);
    },

    addImage(slug, file, onUploadProgress) {
        const formData = new FormData();
        formData.append('file', file[0]);
        return client.post(`${baseUrl}/${slug}/image`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            onUploadProgress,
        });
    },

    delete(slug) {
        return client.delete(`${baseUrl}/${slug}`);
    },
};
