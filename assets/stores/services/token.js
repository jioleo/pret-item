const USER = 'user';
class TokenService {
    getLocalRefreshToken() {
        const user = JSON.parse(localStorage.getItem(USER));
        return user?.refresh_token;
    }

    getLocalAccessToken() {
        const user = JSON.parse(localStorage.getItem(USER));
        return user?.token;
    }

    updateLocalAccessToken(token) {
        const user = JSON.parse(localStorage.getItem(USER));
        user.token = token;
        localStorage.setItem(USER, JSON.stringify(user));
    }

    getUser() {
        return JSON.parse(localStorage.getItem(USER));
    }

    setUser(user) {
        localStorage.setItem(USER, JSON.stringify(user));
    }

    removeUser() {
        localStorage.removeItem(USER);
    }
}

export default new TokenService();
