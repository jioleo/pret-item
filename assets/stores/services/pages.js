function getPageFromHydraString(string) {
    if (string) {
        return parseInt(string.substring(string.length - 1), 10);
    }
    return 1;
}

export default (data) => ({
    totalItems: data['hydra:totalItems'],
    lastPage: getPageFromHydraString('hydra:view' in data ? data['hydra:view']['hydra:last'] : null),
});
