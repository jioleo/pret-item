import { defineStore } from 'pinia';
import { ref } from 'vue';
import { useLoginStore } from '@/stores/auth';

export const useMenusStore = defineStore('menu', () => {
    const appBrand = ref({
        name: 'Pret Items',
        route: '/',
        logo: 'https://getbootstrap.com/docs/5.3/assets/brand/bootstrap-logo.svg',
    });

    const navMenus = ref([
        {
            title: 'Catalog',
            route: { name: 'catalog' },
        },
        {
            title: 'Mes Objets',
            route: { name: 'profile-objects' },
        },
    ]);

    const loginStore = useLoginStore();
    const { logout } = loginStore;
    const userMenus = ref([
        {
            title: 'Se déconnecter',
            action: () => logout(),
        },
    ]);

    const sideBarMenu = ref([]);

    const breadcrumbs = ref([]);

    function addBreadcrumb(title, route) {
        breadcrumbs.value.push({ title, route });
    }

    function removeBreadcrumbAt(index) {
        breadcrumbs.value.splice(index, 1);
    }

    function clearBreadcrumbs() {
        breadcrumbs.value = [];
    }

    return {
        appBrand,
        navMenus,
        userMenus,
        breadcrumbs,
        addBreadcrumb,
        removeBreadcrumbAt,
        clearBreadcrumbs,
    };
});
