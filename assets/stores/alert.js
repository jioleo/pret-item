import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useAlertStore = defineStore('alert', () => {
    const alert = ref();

    function success(message) {
        alert.value = { message, type: 'alert-success' };
    }
    function error(errorCatch) {
        if (errorCatch.message !== 'Failed to refresh token') {
            console.log(errorCatch.message);
            alert.value = { errorCatch, type: 'alert-danger' };
        }
    }
    function clear() {
        alert.value = null;
    }

    return {
        alert, success, error, clear,
    };
});
