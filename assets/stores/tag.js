import { defineStore } from 'pinia';
import api from '@/stores/services/api/tags';
import extractPagesData from '@/stores/services/pages';
import { useAlertStore } from '@/stores/alert';

export const useTagStore = defineStore('tag', () => {
    const tagsState = ref([]);
    const tags = computed(() => tagsState.value);
    const totalItems = ref(0);
    const currentPage = ref(0);
    const lastPage = ref(0);
    const loadingState = ref(false);
    const loading = computed(() => loadingState.value);
    const { error: alertError } = useAlertStore();
    function toggleLoading() {
        loadingState.value = !loadingState.value;
    }

    async function getCollection(params) {
        try {
            toggleLoading();
            const data = await api.getCollection(params)
                .then((r) => r.data);

            tagsState.value = data['hydra:member'];
            // Page
            currentPage.value = params.page ?? 1;
            const pagesData = extractPagesData(data);
            totalItems.value = pagesData.totalItems;
            lastPage.value = pagesData.lastPage;
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    const tagState = ref({});
    const tag = computed(() => tagState.value);

    async function getOneBySlug(slug) {
        try {
            toggleLoading();
            tagState.value = await api.getOneBySlug(slug)
                .then((r) => r.data);
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    async function add(obj) {
        try {
            toggleLoading();
            await api.add(obj);
            tagState.value.push(obj);
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    return {
        tags,
        tag,
        loading,
        totalItems,
        currentPage,
        lastPage,
        getCollection,
        add,
        getOneBySlug,
    };
});
