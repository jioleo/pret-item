import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import api from '@/stores/services/api/auth';
import usersApi from '@/stores/services/api/users';
import { useAlertStore } from '@/stores/alert';
import tokenService from '@/stores/services/token';

import router from '@/router';

export const useLoginStore = defineStore('login', () => {
    const { error: alertError } = useAlertStore();
    const returnUrl = ref('/');
    const authUser = ref(tokenService.getUser());
    const user = computed(() => authUser.value);

    const alertStore = useAlertStore();
    async function login(email, password) {
        try {
            const { data, headers } = await api.login(email, password);

            data['@id'] = headers.location;
            // Requesting the user's data
            usersApi.getOneByIri(data['@id']).then((response) => {
                authUser.value = { ...response.data, ...authUser.value };
                tokenService.setUser(authUser.value);
            }).catch((error) => alertError(error));

            tokenService.setUser(data);
            authUser.value = data;
            router.push(returnUrl.value);
        } catch (error) {
            alertError(error);
        }
    }

    async function refreshToken() {
        try {
            const { data } = await api.refresh(tokenService.getLocalRefreshToken());
            tokenService.updateLocalAccessToken(data.token);
            authUser.value.token = data.token;
        } catch (error) {
            // Ignore
        }
    }

    function logout() {
        authUser.value = null;
        tokenService.removeUser();
        router.push({ name: 'login' });
    }

    return {
        user, returnUrl, login, refreshToken, logout,
    };
});
