import { defineStore } from 'pinia';
import api from '@/stores/services/api/items';
import extractPagesData from '@/stores/services/pages';
import { useAlertStore } from '@/stores/alert';

export const useItemStore = defineStore('item', () => {
    const router = useRouter();
    const itemsState = ref([]);
    const items = computed(() => itemsState.value);
    const totalItems = ref(0);
    const currentPage = ref(0);
    const lastPage = ref(0);
    const loading = ref(false);
    function toggleLoading() {
        loading.value = !loading.value;
    }
    const { error: alertError } = useAlertStore();

    async function getCollection(params) {
        try {
            toggleLoading();
            const data = await api.getCollection(params)
                .then((r) => r.data);

            itemsState.value = data['hydra:member'];
            // page
            currentPage.value = params.page;
            const pagesData = extractPagesData(data);
            totalItems.value = pagesData.totalItems;
            lastPage.value = pagesData.lastPage;
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    async function getCatalog(params) {
        try {
            toggleLoading();
            const data = await api.getCatalog(params)
                .then((r) => r.data);

            itemsState.value = data['hydra:member'];
            // page
            currentPage.value = params.page;
            const pagesData = extractPagesData(data);
            totalItems.value = pagesData.totalItems;
            lastPage.value = pagesData.lastPage;
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    const itemState = ref({});
    const item = computed(() => itemState.value);

    async function getOneBySlug(slug) {
        try {
            toggleLoading();
            itemState.value = await api.getOneBySlug(slug).then((r) => r.data);
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    function fromFullItemToApiItem(obj) {
        obj = {
            '@id': obj['@id'],
            id: obj.id,
            name: obj.name,
            slug: obj.slug,
            description: obj.description,
            tags: obj.tags?.map((tag) => tag['@id']),
            borrower: obj.borrower ? obj.borrower['@id'] : null,
            owner: obj.owner['@id'],
            image: obj.image,
        };
        return obj;
    }

    function addImage(slug, image, onUploadProgress) {
        try {
            toggleLoading();
            api.addImage(slug, image, onUploadProgress)
                .then((r) => { itemState.value = r.data; })
                .catch((err) => alertError(err))
                .finally(() => router.push({ name: 'items-edit', params: { slug: itemState.value.slug } }));
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    async function add(obj, onUploadProgress) {
        try {
            toggleLoading();
            itemState.value = await api.add(fromFullItemToApiItem(obj)).then((r) => r.data);
            addImage(itemState.value.slug, obj.image, onUploadProgress);
            itemsState.value.push(obj);
        } catch (error) {
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    async function edit(obj, onUploadProgress) {
        try {
            toggleLoading();
            itemState.value = await api.edit(fromFullItemToApiItem(obj)).then((r) => r.data);
            router.push({ name: 'items-detail', params: { slug: itemState.value.slug } });
            if (obj.image) {
                addImage(itemState.value.slug, obj.image, onUploadProgress);
            }
        } catch (error) {
            itemState.value.error = error.response ? error.response : error;
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    async function remove(obj) {
        try {
            toggleLoading();
            await api.delete(obj.value.slug);
            const index = itemsState.value.indexOf(obj);
            if (index > -1) {
                itemsState.value.splice(index, 1);
            }
            router.push({ name: 'profile-objects' });
        } catch (error) {
            itemState.value.error = error.response ? error.response : error;
            alertError(error);
        } finally {
            toggleLoading();
        }
    }

    return {
        items,
        item,
        loading,
        totalItems,
        currentPage,
        lastPage,
        getCollection,
        getCatalog,
        getOneBySlug,
        addImage,
        add,
        edit,
        remove,
    };
});
