import { defineStore } from 'pinia';
import { computed } from 'vue';

export const useConfigStore = defineStore('config', () => {
    const appName = computed(() => 'PretItem');
    const year = computed(() => '2023');

    return {
        appName, year,
    };
});
