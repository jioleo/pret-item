import 'vuetify/styles';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import '@mdi/font/css/materialdesignicons.css';
import { myCustomLightTheme, myCustomDarkTheme } from '@/assets/styles/vuetify-themes';

const vuetify = createVuetify({
    components,
    directives,
    defaults: {
        global: {
            // variant: 'solo',
        },
    },
    theme: {
        themes: {
            light: myCustomLightTheme,
            dark: myCustomDarkTheme,
        },
    },
});

export default vuetify;
