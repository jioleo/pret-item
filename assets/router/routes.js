import CatalogView from '@/views/CatalogView';
import FriendView from '@/views/FriendView';
import UserView from '@/views/UserView';
import LoginView from '@/views/LoginView';
import RegisterView from '@/views/RegisterView';
import MyItemsView from '@/views/MyItemsView';
import ItemAddView from '@/views/ItemAddView';
import ItemEditView from '@/views/ItemEditView';
import ItemDetailView from '@/views/ItemDetailView';
import TagDetailView from '@/views/TagDetailView';

const routes = [
    {
        path: '/',
        name: 'home',
        redirect: { name: 'catalog' },
    },
    {
        path: '/catalog',
        name: 'catalog',
        component: CatalogView,
        props: (route) => ({ page: Number.parseInt(route.query.page ?? 1, 10) }),
        meta: {
            layout: 'Default',
        },
    },
    {
        path: '/profile/objects',
        name: 'profile-objects',
        component: MyItemsView,
        meta: {
            layout: 'Default',
        },
    },
    {
        path: '/items',
        name: 'items',
        children: [
            {
                path: ':slug',
                name: 'items-detail',
                component: ItemDetailView,
                props: (route) => ({ slug: route.params.slug }),
            },
            {
                path: 'add',
                name: 'items-add',
                component: ItemAddView,
            },
            {
                path: 'edit/:slug',
                name: 'items-edit',
                component: ItemEditView,
                props: (route) => ({ slug: route.params.slug }),
            },
        ],
        meta: {
            layout: 'Default',
        },
    },
    {
        path: '/friends',
        name: 'friends',
        component: FriendView,
    },
    {
        path: '/tags/:slug',
        name: 'tags-detail',
        component: TagDetailView,
        props: (route) => ({
            slug: route.params.slug,
        }),
        meta: {
            layout: 'Default',
        },
    },
    {
        path: '/users',
        name: 'users',
        children: [
            {
                path: ':pseudo',
                name: 'users-detail',
                component: UserView,
                props: (route) => ({
                    slug: route.params.pseudo,
                }),
            },
        ],
        meta: {
            layout: 'Default',
        },
    },
    {
        path: '/login',
        name: 'login',
        component: LoginView,
        meta: {
            layout: 'Default',
        },
    },
    {
        path: '/signup',
        name: 'signup',
        component: RegisterView,
        meta: {
            layout: 'Default',
        },
    },
];

export default routes;
