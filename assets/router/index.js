import { createRouter, createWebHistory } from 'vue-router';
import { useLoginStore } from '@/stores/auth';
import routes from './routes';
import { useAlertStore } from '@/stores/alert';

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior() {
        document.getElementsByTagName('BODY')[0].scrollIntoView({ behavior: 'smooth' });
    },
});

router.beforeEach((to) => {
    // clear alert on route change
    const alertStore = useAlertStore();
    alertStore.clear();

    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['login', 'signup'];
    const authRequired = !publicPages.includes(to.name);
    const auth = useLoginStore();

    if (authRequired && !auth.user) {
        auth.returnUrl = to.fullPath;
        return { name: 'login' };
    }
});

export default router;
