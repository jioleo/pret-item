export const myCustomLightTheme = {
    dark: false,
    colors: {
        background: '#FFFFFF',
        surface: '#FFFFFF',
        primary: '#6200EE',
        'primary-darken-1': '#3700B3',
        secondary: '#03DAC6',
        'secondary-darken-1': '#018786',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    },
};
// P1
// #DCDFDA
// #C8D6A2
// #B7CE66
// #8FB43A
// #4B5943
// P2
// #24D26D
// #282E2E
// #FFFFFF
// #AB47A3
// #525351
// P3
// #343330;
// #45462a;
// #7e5920;
// #dc851f;
// #ffa737;

export const myCustomDarkTheme = {
    dark: true,
    colors: {
        background: '#282E2E',
        surface: '#525351',
        primary: '#AB47A3',
        secondary: '#24D26D',
        error: '#ea868f',
        info: '#6edff6',
        success: '#B7CE66',
        warning: '#ffda6a',
    },
};
