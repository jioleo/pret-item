import LayoutDefault from '@/layouts/LayoutDefault';
import LayoutSidebar from '@/layouts/LayoutSidebar';

export function registerLayouts(app) {
    app.component('LayoutDefault', LayoutDefault);
    app.component('LayoutSidebar', LayoutSidebar);
}
