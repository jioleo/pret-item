<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230603234359 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE friendship DROP CONSTRAINT fk_7234a45fa7f43455');
        $this->addSql('DROP INDEX idx_7234a45fa7f43455');
        $this->addSql('ALTER TABLE friendship ADD status VARCHAR(12) NOT NULL');
        $this->addSql('ALTER TABLE friendship ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE friendship ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE friendship DROP accepted');
        $this->addSql('ALTER TABLE friendship RENAME COLUMN requestor_id TO requester_id');
        $this->addSql('ALTER TABLE friendship ADD CONSTRAINT FK_7234A45FED442CF4 FOREIGN KEY (requester_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7234A45FED442CF4 ON friendship (requester_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1F1B251E989D9B62 ON item (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_389B783989D9B62 ON tag (slug)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_389B783989D9B62');
        $this->addSql('ALTER TABLE friendship DROP CONSTRAINT FK_7234A45FED442CF4');
        $this->addSql('DROP INDEX IDX_7234A45FED442CF4');
        $this->addSql('ALTER TABLE friendship ADD accepted BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE friendship DROP status');
        $this->addSql('ALTER TABLE friendship DROP created_at');
        $this->addSql('ALTER TABLE friendship DROP updated_at');
        $this->addSql('ALTER TABLE friendship RENAME COLUMN requester_id TO requestor_id');
        $this->addSql('ALTER TABLE friendship ADD CONSTRAINT fk_7234a45fa7f43455 FOREIGN KEY (requestor_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7234a45fa7f43455 ON friendship (requestor_id)');
        $this->addSql('DROP INDEX UNIQ_1F1B251E989D9B62');
    }
}
