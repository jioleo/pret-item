const Encore = require('@symfony/webpack-encore');
const path = require('path');
const webpack = require('webpack');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    .addEntry('app', './assets/main.js')
    .addEntry('admin', './assets/admin.js')
    // This is our alias to the root vue components dir
    .addAliases({
        '@': path.resolve(__dirname, 'assets'),
        styles: path.resolve(__dirname, 'assets', 'assets', 'styles'),
    })
    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()
    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    // enables and configure @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = '3.23';
    })

    // .copyFiles({
    //     from: './assets/images',
    //     to: Encore.isProduction()
    //         ? 'images/[path][name].[hash:8].[ext]'
    //         : 'images/[path][name].[ext]',
    // })
    // Enable .vue file processing
    .enableVueLoader(() => {}, { runtimeCompilerBuild: false })
    // gives better module CSS naming in dev
    .configureCssLoader((config) => {
        if (!Encore.isProduction() && config.modules) {
            config.modules.localIdentName = '[name]_[local]_[hash:base64:5]';
        }
    })
    // enables Sass/SCSS support
    .enableSassLoader()

    .addPlugin(new webpack.DefinePlugin({
        __VUE_OPTIONS_API__: true,
        __VUE_PROD_DEVTOOLS__: false,
    }))
    .addPlugin(require('unplugin-vue-components/webpack')({
        dirs: [
            path.resolve('assets/components'),
            path.resolve('assets/views'),
        ],
        extensions: ['vue', 'js'],
        deep: true,
    }))
    .addPlugin(require('unplugin-auto-import/webpack')({
        include: [
            /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
            /\.vue$/, /\.vue\?vue/, // .vue
        ],
        imports: [
            // presets
            // 'vue',
            // custom
            {
                vue: [
                    // named imports
                    'ref', // import { ref } from 'vue',
                    'computed',
                    'onMounted',
                    'onBeforeMount',
                    'watch',
                ],
                'vue-router': [
                    'onBeforeRouteUpdate',
                    'useRouter',
                    'useRoute',
                ],
                pinia: [
                    'storeToRefs',
                ],
                axios: [
                    // default imports
                    ['default', 'axios'], // import { default as axios } from 'axios',
                ],
            },
        ],
        eslintrc: {
            enabled: true,
        },
    }));

if (!Encore.isProduction()) {
    Encore.disableCssExtraction();
}

module.exports = Encore.getWebpackConfig();
