<?php

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    if ('dev' == $context['APP_ENV']) {
        umask(0002);
    }

    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
