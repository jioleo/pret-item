# Executables (local)
DOCKER_COMP = docker compose

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec php
NODE_CONT = $(DOCKER_COMP) exec node

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
SYMFONY  = $(PHP) bin/console

# Misc
.DEFAULT_GOAL = help
.PHONY        : help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Symfony Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images   option:   --pull --no-cache
	@$(DOCKER_COMP) build --pull --no-cache

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

logs.node: ## Show live logs
	@$(DOCKER_COMP) logs node --tail=0 --follow

sh: ## Connect to the PHP FPM container
	@$(PHP_CONT) sh

rights:
	docker-compose run --rm php chown -R $(id -u):$(id -g) .
## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-dev --no-progress --no-scripts --no-interaction
vendor: composer

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf

admin.crud:
	$(SYMFONY) make:admin:crud

form:
	$(SYMFONY) make:form

## —— Base de donnée 💽 ————————————————————————————————————————————————————————
entity:
	$(SYMFONY) make:entity

migration:
	$(SYMFONY) make:migration

migrate:
	$(SYMFONY) doctrine:migrations:migrate -n

db.recreate: delImages db.drop db.create migrate fixtures ## Commande pour recréer la BDD de ZERO !

db.drop:
	$(SYMFONY) doctrine:database:drop -f

db.create:
	$(SYMFONY) doctrine:database:create

fixtures: ## (Attention vide la BDD ! )
	$(SYMFONY) doctrine:fixtures:load -n

delImages:
	rm -f ./public/images/*.jpg

## —— NodeJS ————————————————————————————————————————————————————————————————————
node.sh: ##
	@$(NODE_CONT) sh

npm.watch: ##
	@$(NODE_CONT) npm run watch

npm.install: ##
	@$(eval c ?=)
	@$(NODE_CONT) npm i $(c) -D

npm.remove: ##
	@$(eval c ?=)
	@$(NODE_CONT) npm remove $(c)

npm: ##
	@$(eval c ?=)
	@$(NODE_CONT) npm $(c)

## —— Check ————————————————————————————————————————————————————————————————————
check:
	@$(COMPOSER) check

fix:
	@$(COMPOSER) fix
