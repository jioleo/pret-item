<?php

namespace App\ApiPlatform\OpenApi;

use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\MediaType;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\OpenApi;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

#[AsDecorator(decorates: 'api_platform.openapi.factory', priority: -256)]
class OpenApiFactoryDecorator implements OpenApiFactoryInterface
{
    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        // Add Bearer scheme to the Authorization token
        $securitySchemes = $openApi->getComponents()->getSecuritySchemes() ?: new \ArrayObject();
        $securitySchemes['JWT'] = $securitySchemes['JWT']
            ->withType('http')
            ->withScheme('bearer');

        $pathLogin = $openApi->getPaths()->getPath('/api/login');
        $loginOperation = $pathLogin->getPost();


        $response = $loginOperation->getResponses();
        unset($response[200]['content']['application/json']['schema']['properties']['token']);
        $response[200]['content']['application/json']['schema']['properties'] = [
            'token' => [
                'readOnly' => true,
                'type' => 'string',
                'nullable' => false,
            ],
            'refresh_token' => [
                'readOnly' => true,
                'type' => 'string',
                'nullable' => false,
            ]
        ];

        // Add Location on login response header
        $response[200]['headers'] = [
            'Location' => [
                'schema' => [
                    'type' => 'string',
                    'example' => "/api/users/{id}",
                ],
                'description' => 'The user Iri location',
            ]
        ];

        // Edit jwt login path
        $openApi
            ->getPaths()
            ->addPath('/api/login', $pathLogin->withPost(
                $loginOperation
                ->withResponses($response)
            )
        );

        // Add refresh token path to swagger
        unset($response[200]['headers']);
        $openApi
            ->getPaths()
            ->addPath('/token/refresh', (new PathItem())->withPost(
                (new Operation())
                ->withOperationId('api_refresh_token')
                ->withTags(['Login Check'])
                ->withResponses($response)
                ->withSummary('Refresh the user token.')
                ->withRequestBody(
                    (new RequestBody())
                    ->withDescription('The refresh token data')
                    ->withContent(new \ArrayObject([
                        'application/json' => new MediaType(new \ArrayObject(new \ArrayObject([
                            'type' => 'object',
                            'properties' => $properties = $properties = $this->getJsonSchemaFromPathParts(explode('.', 'refresh_token')),
                            'required' => array_keys($properties),
                        ]))),
                    ]))
                    ->withRequired(true)
                )
            )
        );

        return $openApi;
    }

    private function getJsonSchemaFromPathParts(array $pathParts): array
    {
        $jsonSchema = [];

        if (count($pathParts) === 1) {
            $jsonSchema[array_shift($pathParts)] = [
                'type' => 'string',
                'nullable' => false,
            ];

            return $jsonSchema;
        }

        $pathPart = array_shift($pathParts);
        $properties = $this->getJsonSchemaFromPathParts($pathParts);
        $jsonSchema[$pathPart] = [
            'type' => 'object',
            'properties' => $properties,
            'required' => array_keys($properties),
        ];

        return $jsonSchema;
    }
}
