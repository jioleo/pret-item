<?php

namespace App\ApiPlatform\Serializer;

use App\Entity\Image;
use App\Entity\Item;
use App\Entity\User;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

final class ImageNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'IMAGE_NORMALIZER_ALREADY_CALLED';

    public function __construct(private StorageInterface $storage)
    {
    }

    public function normalize($object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context[self::ALREADY_CALLED] = true;

        switch (get_class($object)) {
            case Image::class:
                $image = $object;
                break;
            case Item::class:
                assert($object instanceof Item);
                $image = $object->getImage();
                if ($object->getOwner()) {
                    $avatar = $object->getOwner()->getAvatar();
                    $avatar->contentUrl = $this->storage->resolveUri($avatar, 'file');
                }
                if ($object->getBorrower()) {
                    $avatar = $object->getBorrower()->getAvatar();
                    $avatar->contentUrl = $this->storage->resolveUri($avatar, 'file');
                }
                break;
            case User::class:
                $image = $object->getAvatar();
                break;
        }

        if ($image) {
            $image->contentUrl = $this->storage->resolveUri($image, 'file');
        }

        return $this->normalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Image || $data instanceof User || $data instanceof Item;
    }
}