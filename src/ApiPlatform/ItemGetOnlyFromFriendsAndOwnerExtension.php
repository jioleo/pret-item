<?php

namespace App\ApiPlatform;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\Item;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;

// User can see only friends
class ItemGetOnlyFromFriendsAndOwnerExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly UserRepository $userRepository,
    )
    {
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        $this->addWhereOwnerIsFriendsOrUser($context, $resourceClass, $queryBuilder);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, ?Operation $operation = null, array $context = []): void
    {
        $this->addWhereOwnerIsFriendsOrUser($context, $resourceClass, $queryBuilder);
    }

    private function addWhereOwnerIsFriendsOrUser(array $context, string $resourceClass, QueryBuilder $queryBuilder): void
    {
        if (Item::class !== $resourceClass || $this->security->isGranted('ROLE_ADMIN')) {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];

        $user = $this->security->getUser();
        assert($user instanceof User);
        if ($user) {
            $userList = $this->userRepository->findFriendsByUser($user);
            $userIdList = array_map(fn ($user) => $user->getId(), $userList);
            if ($context['operation_name'] !== 'item_catalog') {
                $userIdList[] = $user->getId();
            }
            $queryBuilder
                ->andWhere($queryBuilder->expr()->orX(
                    $queryBuilder->expr()->in(sprintf('%s.owner', $rootAlias), $userIdList),
                    $queryBuilder->expr()->eq(sprintf('%s.borrower', $rootAlias), $user->getId())
                ))
            ;
        }
    }
}
