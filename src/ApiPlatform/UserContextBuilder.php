<?php

namespace App\ApiPlatform;

use ApiPlatform\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Security\Voter\UserVoter;
use Symfony\Bundle\SecurityBundle\Security;

final class UserContextBuilder implements SerializerContextBuilderInterface
{

    public function __construct(
        private SerializerContextBuilderInterface $decorated,
        private Security $security
    ) {
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        if ($resourceClass === User::class
            && isset($context['groups'])
            && $this->security->isGranted(UserVoter::EMAIL, $resourceClass)
        )
        {
            $context['groups'][] = 'user:secure';
        }

        return $context;
    }
}