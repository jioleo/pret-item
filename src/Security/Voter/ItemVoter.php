<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Entity\Item;
use App\Repository\FriendshipRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ItemVoter extends Voter
{
    // these strings are just invented: you can use anything
    public const EDIT = 'EDIT';
    public const VIEW = 'VIEW';
    public const VIEW_ALL = 'VIEW_ALL';
    public const VIEW_FROM_FRIENDS = 'VIEW_FROM_FRIENDS';
    public const DELETE = 'DELETE';

    public function __construct(
        private Security $security,
        private FriendshipRepository $friendshipRepository,
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return $subject instanceof Item && in_array($attribute, [
            self::VIEW,
            self::VIEW_ALL,
            self::VIEW_FROM_FRIENDS,
            self::EDIT,
            self::DELETE
            ]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $currentUser = $token->getUser();

        if (!$currentUser instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // Admin à tous les droits
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        assert($subject instanceof Item);
        $item = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->isAllowedView($item, $currentUser);
                break;
            case self::VIEW_ALL:
                return true;
                break;
            case self::VIEW_FROM_FRIENDS:
                return $this->security->isGranted('ROLE_USER');
                break;
            case self::EDIT:
                return $item->getOwner() === $currentUser;
                break;
            case self::DELETE:
                return $item->getOwner() === $currentUser;
                break;
        }
    }

    private function isAllowedView(Item $item, User $user): bool
    {
        return $item->getOwner() === $user;
    }
}