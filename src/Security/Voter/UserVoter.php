<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Repository\FriendshipRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    // these strings are just invented: you can use anything
    public const EMAIL = 'USER_SHOW_EMAIL';
    public const EDIT = 'USER_EDIT';
    public const DELETE = 'USER_DELETE';
    public const VIEW = 'USER_VIEW';

    public function __construct(
        private Security $security,
        private FriendshipRepository $friendshipRepository
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return in_array($attribute, [self::EMAIL, self::VIEW, self::DELETE, self::EDIT])
            && $subject instanceof User;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $currentUser = $token->getUser();

        if (!$currentUser instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // Admin à tous les droits
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        assert($subject instanceof User);
        $user = $subject;

        switch ($attribute) {
            case self::EMAIL:
                return $user === $currentUser;
                break;
            case self::VIEW:
                return $this->isAllowedToView($user, $currentUser);
                break;
            case self::EDIT:
                return $user === $currentUser;
                break;
            case self::DELETE:
                return $user === $currentUser;
                break;
        }
    }

    private function isAllowedToView(User $user, User $currentUser): bool
    {
        // $friendsOfUser = $this->friendshipRepository->findFriendsByUser($user);
        // autorisé si est l'utilisateur courant ou si est dans sa liste d'amis
        return $user === $currentUser
            // || in_array($currentUser, $friendsOfUser)
        ;
    }
}