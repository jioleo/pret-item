<?php

namespace App\Security\Voter;

use App\Entity\Friendship;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FriendshipVoter extends Voter
{
    public const EDIT = 'FRIENDSHIP_EDIT';
    public const DELETE = 'FRIENDSHIP_DELETE';

    public function __construct(
        private Security $security,
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::EDIT, self::DELETE])
            && $subject instanceof Friendship;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        // Admin à tous les droits
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        assert($subject instanceof Friendship);
        $friendship = $subject;

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                return $this->isAllowedToEdit($friendship, $user);
                break;
            case self::DELETE:
                return $this->isAllowedToDelete($friendship, $user);
                break;
        }

        return false;
    }

    private function isAllowedToDelete(Friendship $friendship, User $user): bool
    {
        if (!$friendship->isAccepted() && $user === $friendship->getReceiver()) {
            return true;
        }
        if ($friendship->isAccepted() && ($user === $friendship->getReceiver() || $user === $friendship->getrequester() )) {
            return true;
        }
        return false;
    }

    private function isAllowedToEdit(Friendship $friendship, User $user): bool
    {
        return !$friendship->isAccepted() && $user === $friendship->getReceiver();
    }
}
