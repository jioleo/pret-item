<?php

namespace App\EventSubscriber;

use ApiPlatform\Api\IriConverterInterface;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: "lexik_jwt_authentication.on_authentication_success", method: 'onLoginSuccess', priority: -256)]
class LoginSuccessListener
{
    public function __construct(private IriConverterInterface $iriConverter)
    {
        
    }


    public function onLoginSuccess(AuthenticationSuccessEvent $event): void
    {
        /** @var User */
        $user = $event->getUser();
        if (!$user instanceof User) {
            return;
        }

        $event->getResponse()->headers->set('Location', $this->iriConverter->getIriFromResource($user));
    }
}