<?php

namespace App\EasyAdmin\Field;

use App\EasyAdmin\Form\ImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Asset;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

final class VichImageField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = 'Image'): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTextAlign('center')
            // this template is used in 'index' and 'detail' pages
            ->setTemplatePath('admin/fields/vich_image.html.twig')
            // this is used in 'edit' and 'new' pages to edit the field contents
            ->setFormType(ImageType::class)
            ->setRequired(false)
            ->addCssClass('field-image field-vich-image')
            ->addJsFiles(Asset::fromEasyAdminAssetPackage('field-image.js'), Asset::fromEasyAdminAssetPackage('field-file-upload.js'))
        ;
    }
}
