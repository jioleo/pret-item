<?php

namespace App\EasyAdmin\Filter\Configurator;

use App\EasyAdmin\Filter\AutocompleteEntityFilterType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterConfiguratorInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\DependencyInjection\Attribute\AsTaggedItem;

#[AsTaggedItem(index: 'ea.filter_configurator', priority: 1)]
final class EntityFilterConfigurator implements FilterConfiguratorInterface
{
    private AdminUrlGenerator $adminUrlGenerator;

    public function __construct(AdminUrlGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public function supports(
        FilterDto $filterDto,
        ?FieldDto $fieldDto,
        EntityDto $entityDto,
        AdminContext $context
    ): bool {
        return EntityFilter::class === $filterDto->getFqcn();
    }

    public function configure(
        FilterDto $filterDto,
        ?FieldDto $fieldDto,
        EntityDto $entityDto,
        AdminContext $context
    ): void {
        $propertyName = $filterDto->getProperty();
        if (!$entityDto->isAssociation($propertyName)) {
            return;
        }

        if ($fieldDto && $fieldDto->getCustomOption(AssociationField::OPTION_AUTOCOMPLETE)) {
            $doctrineMetadata = $entityDto->getPropertyMetadata($propertyName);
            $filterDto->setFormTypeOptionIfNotSet('value_type_options.attr.data-ea-widget', 'ea-autocomplete');

            $targetEntityFqcn = $doctrineMetadata->get('targetEntity');
            $targetCrudControllerFqcn = $context->getCrudControllers()->findCrudFqcnByEntityFqcn($targetEntityFqcn);

            if ($targetCrudControllerFqcn) {
                $filterDto->setFormTypeOptionIfNotSet('value_type', AutocompleteEntityFilterType::class);

                $autocompleteEndpointUrl = $this->adminUrlGenerator
                    ->set('page', 1)
                    ->setController($targetCrudControllerFqcn)
                    ->setAction('autocomplete')
                    ->setEntityId(null)
                    ->unset(EA::SORT)
                    ->set('autocompleteContext', [
                        EA::CRUD_CONTROLLER_FQCN => $context->getRequest()->query->get(EA::CRUD_CONTROLLER_FQCN),
                        'propertyName' => $propertyName,
                        'originatingPage' => $context->getCrud()->getCurrentAction(),
                    ])
                    ->generateUrl();
                $filterDto->setFormTypeOption(
                    'value_type_options.attr.data-ea-autocomplete-endpoint-url',
                    $autocompleteEndpointUrl
                );
            }
        }
    }
}
