<?php

namespace App\DataFixtures;

use App\Factory\TagFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $base = ['Livre', 'Genre', 'Jeux de société', 'Jeux vidéo', 'Types de jeu', 'Outils'];
        $livreArray = ['BD', 'Encyclopédie', 'Manga', 'Roman'];
        $genreArray = ['Science-Fiction', 'Fantasie'];
        $typeJeuArray = ['Stratégie', 'Jeux de cartes'];

        foreach ($base as $tagName) {
            $tag = TagFactory::new([
                'name' => $tagName
            ])->create();

            if ($tag->getName() === 'Livre') {
                foreach ($livreArray as $tagName) {
                    TagFactory::new([
                        'name' => $tagName,
                        'parent' => $tag
                    ])->create();
                }
            }

            if ($tag->getName() === 'Genre') {
                foreach ($genreArray as $tagName) {
                    TagFactory::new([
                        'name' => $tagName,
                        'parent' => $tag
                    ])->create();
                }
            }

            if ($tag->getName() === 'Types de jeu') {
                foreach ($typeJeuArray as $tagName) {
                    TagFactory::new([
                        'name' => $tagName,
                        'parent' => $tag
                    ])->create();
                }
            }
        }
    }
}
