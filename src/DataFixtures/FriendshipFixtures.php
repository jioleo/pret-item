<?php

namespace App\DataFixtures;

use App\Entity\Friendship;
use App\Factory\FriendshipFactory;
use App\Factory\UserFactory;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FriendshipFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function __construct() {
        parent::__construct();
    }

    public function load(ObjectManager $manager): void
    {
        $users = UserFactory::all();
        foreach ($users as $user) {
            $this->faker->unique($reset = true);
            $friends = UserFactory::randomRange(0, count($users) -1);
            foreach ($friends as $friend) {
                if ($user->getId() !== $friend->getId()) {
                    FriendshipFactory::new([
                        'requester' => $user,
                        'receiver' => $friend,
                    ])->create();
                }
            }
        }
    }

    /*
     * @return array<class-string<FixtureInterface>>
     */
    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
