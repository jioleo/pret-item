<?php

namespace App\DataFixtures;

use App\Factory\ItemFactory;
use App\Factory\UserFactory;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ItemFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function __construct() {
        parent::__construct();
    }

    public function load(ObjectManager $manager): void
    {
        ItemFactory::createMany($this->faker->numberBetween(200, 300));
    }

    /*
     * @return array<class-string<FixtureInterface>>
     */
    public function getDependencies(): array
    {
        return [
            TagFixtures::class,
            UserFixtures::class,
        ];
    }
}
