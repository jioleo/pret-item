<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\UserFactory;
use App\Repository\ItemRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends AbstractFixtures
{
    public function __construct() {
        parent::__construct();
    }

    public function load(ObjectManager $manager): void
    {
        // Create Admin user.
        UserFactory::createOne([
            'pseudo' => 'admin',
            'email' => 'admin@noreply.local',
            'plainPassword' => 'test',
            'roles' => [
                'ROLE_ADMIN'
            ]
        ]);
        // Create Normal user.
        UserFactory::createOne([
            'pseudo' => 'user',
            'email' => 'user@noreply.local',
            'plainPassword' => 'test',
        ]);

        // Create other users.
        UserFactory::createMany(28);
    }
}
