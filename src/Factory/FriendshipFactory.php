<?php

namespace App\Factory;

use App\Entity\Friendship;
use App\Repository\FriendshipRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Friendship>
 *
 * @method        Friendship|Proxy create(array|callable $attributes = [])
 * @method static Friendship|Proxy createOne(array $attributes = [])
 * @method static Friendship|Proxy find(object|array|mixed $criteria)
 * @method static Friendship|Proxy findOrCreate(array $attributes)
 * @method static Friendship|Proxy first(string $sortedField = 'id')
 * @method static Friendship|Proxy last(string $sortedField = 'id')
 * @method static Friendship|Proxy random(array $attributes = [])
 * @method static Friendship|Proxy randomOrCreate(array $attributes = [])
 * @method static FriendshipRepository|RepositoryProxy repository()
 * @method static Friendship[]|Proxy[] all()
 * @method static Friendship[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Friendship[]|Proxy[] createSequence(iterable|callable $sequence)
 * @method static Friendship[]|Proxy[] findBy(array $attributes)
 * @method static Friendship[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Friendship[]|Proxy[] randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<Friendship> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<Friendship> createOne(array $attributes = [])
 * @phpstan-method static Proxy<Friendship> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<Friendship> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<Friendship> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<Friendship> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<Friendship> random(array $attributes = [])
 * @phpstan-method static Proxy<Friendship> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<Friendship> repository()
 * @phpstan-method static list<Proxy<Friendship>> all()
 * @phpstan-method static list<Proxy<Friendship>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<Friendship>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<Friendship>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<Friendship>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<Friendship>> randomSet(int $number, array $attributes = [])
 */
final class FriendshipFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'status' => self::faker()->optional(0.3, Friendship::STATUS_FRIEND)->randomElement([Friendship::STATUS_INVITED, Friendship::STATUS_REJECTED]),
            'receiver' => UserFactory::new()->random(),
            'requester' => UserFactory::new()->random(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Friendship $friendship): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Friendship::class;
    }
}
