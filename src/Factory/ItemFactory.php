<?php

namespace App\Factory;

use App\Entity\Item;
use App\Entity\User;
use App\Repository\ItemRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Item>
 *
 * @method        Item|Proxy create(array|callable $attributes = [])
 * @method static Item|Proxy createOne(array $attributes = [])
 * @method static Item|Proxy find(object|array|mixed $criteria)
 * @method static Item|Proxy findOrCreate(array $attributes)
 * @method static Item|Proxy first(string $sortedField = 'id')
 * @method static Item|Proxy last(string $sortedField = 'id')
 * @method static Item|Proxy random(array $attributes = [])
 * @method static Item|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemRepository|RepositoryProxy repository()
 * @method static Item[]|Proxy[] all()
 * @method static Item[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Item[]|Proxy[] createSequence(iterable|callable $sequence)
 * @method static Item[]|Proxy[] findBy(array $attributes)
 * @method static Item[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Item[]|Proxy[] randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<Item> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<Item> createOne(array $attributes = [])
 * @phpstan-method static Proxy<Item> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<Item> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<Item> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<Item> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<Item> random(array $attributes = [])
 * @phpstan-method static Proxy<Item> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<Item> repository()
 * @phpstan-method static list<Proxy<Item>> all()
 * @phpstan-method static list<Proxy<Item>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<Item>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<Item>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<Item>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<Item>> randomSet(int $number, array $attributes = [])
 */
final class ItemFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'createdAt' => self::faker()->dateTimeBetween('-1 year'),
            'name' => self::faker()->words(self::faker()->numberBetween(1, 2), true),
            'description' => self::faker()->text(255),
            'owner' => UserFactory::random(),
            'borrower' => self::faker()->optional(0.6)->passthrough(UserFactory::random()),
            'updatedAt' => self::faker()->dateTimeBetween('-1 year'),
            'tags' => TagFactory::randomRange(1, 3),
            'image' => ImageFactory::new(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Item $item): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Item::class;
    }
}
