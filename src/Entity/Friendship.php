<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use App\Entity\Traits\HasIdTrait;
use App\Entity\Traits\HasTimestampTrait;
use App\Repository\FriendshipRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#ORM
#[ORM\Entity(repositoryClass: FriendshipRepository::class)]
#[UniqueEntity(
    fields: ['requester', 'receiver'],
    errorPath: 'receiver',
    message: 'friendship.unique',
)]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['get'],],
        ),
        new GetCollection(),
        new Post(),
        new Patch(security: "is_granted('FRIENDSHIP_EDIT', object)"),
        new Delete(security: "is_granted('FRIENDSHIP_DELETE', object)")
    ]
)]
class Friendship
{
    use HasIdTrait;
    use HasTimestampTrait;

    const STATUS_INVITED = 'invited';
    const STATUS_FRIEND = 'friend';
    const STATUS_REJECTED = 'rejected';
    const STATUS = [self::STATUS_INVITED, self::STATUS_FRIEND, self::STATUS_REJECTED];

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull(message: 'entity.friendship.requester.notnull')]
    private ?User $requester = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull(message: 'entity.friendship.receiver.notnull')]
    private ?User $receiver = null;

    #[ORM\Column(length: 12)]
    #[Assert\Choice(choices: self::STATUS, message: 'friendship.status.invalid')]
    private ?string $status = self::STATUS_INVITED;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequester(): ?User
    {
        return $this->requester;
    }

    public function setRequester(?User $requester): self
    {
        $this->requester = $requester;

        return $this;
    }

    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
