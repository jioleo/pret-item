<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Traits\HasIdTrait;
use App\Repository\ImageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: ImageRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['image:read']],
    types: ['https://schema.org/image'],
)]
class Image
{
    use HasIdTrait;

    #[ORM\Column(length: 255)]
    private ?string $path = null;

    public ?string $contentUrl = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'images', fileNameProperty: 'path')]
    #[Assert\NotNull(groups: ['image_create'])]
    #[Groups(['item:write', 'user:write'])]
    #[ApiProperty(
        openapiContext: [
            'type' => 'string',
            'format' => 'binary',
        ]
    )]
    private ?File $file = null;

    #[ORM\OneToOne(mappedBy: 'avatar')]
    private ?User $userLinked = null;

    #[ORM\OneToOne(mappedBy: 'image')]
    private ?Item $item = null;

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     */
    public function setFile(File|UploadedFile|null $file): self
    {
        $this->file = $file;

        if (null !== $file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getUserLinked(): ?User
    {
        return $this->userLinked;
    }

    public function setUserLinked(?User $userLinked): self
    {
        // unset the owning side of the relation if necessary
        if ($userLinked === null && $this->userLinked !== null) {
            $this->userLinked->setAvatar(null);
        }

        // set the owning side of the relation if necessary
        if ($userLinked !== null && $userLinked->getAvatar() !== $this) {
            $userLinked->setAvatar($this);
        }

        $this->userLinked = $userLinked;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        // unset the owning side of the relation if necessary
        if ($item === null && $this->item !== null) {
            $this->item->setImage(null);
        }

        // set the owning side of the relation if necessary
        if ($item !== null && $item->getImage() !== $this) {
            $item->setImage($this);
        }

        $this->item = $item;

        return $this;
    }

    public function isUserAllowedToEdit(User $user): bool
    {
        $returnValue = false;
        if ($this->getUserLinked()) {
            $returnValue = $user === $this->getUserLinked();
        }
        if ($this->getItem()) {
            $returnValue = $user === $this->getItem()->getOwner();
        }
        return $returnValue;
    }
}
