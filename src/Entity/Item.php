<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
// use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\Filter\SearchFilter;
use App\ApiPlatform\Filter\SimpleSearchFilter;
use App\Controller\CreateImageAction;
use App\Entity\Traits\HasTimestampTrait;
use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Slug;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['item:write']],
    operations: [
        new Get(
            normalizationContext: [
                'skip_null_values' => false,
                'groups' => ['item:item:get', 'get:name', 'get:slug', 'get:description']
            ],
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['item:collection:get', 'get:name', 'get:created_at', 'get:updated_at', 'get:slug']],
        ),
        new Post(
            security: "is_granted('ROLE_USER')",
            normalizationContext: [
                'skip_null_values' => false,
                'groups' => ['item:item:get', 'get:name', 'get:slug', 'get:description']
            ],
        ),
        new Patch(
            security: "is_granted('EDIT', object)",
            normalizationContext: [
                'skip_null_values' => false,
                'groups' => ['item:item:get', 'get:name', 'get:slug', 'get:description']
            ],
        ),
        new Delete(security: "is_granted('DELETE', object)"),
    ],
)]
#[ApiResource(
    // Catalog
    uriTemplate: '/catalog',
    operations: [
        new GetCollection(
            name: 'item_catalog',
            normalizationContext: ['groups' => ['item:collection:get', 'get:name', 'get:created_at', 'get:updated_at', 'get:slug']],
        ),
    ],
)]
#[ApiResource(
    uriTemplate: 'items/{slug}/image',
    operations: [
        new Post(
            security: "is_granted('EDIT', object)",
            controller: CreateImageAction::class,
            deserialize: false,
            validationContext: ['groups' => ['Default', 'image_create']],
            openapi: new Model\Operation(
                requestBody: new Model\RequestBody(
                    content: new \ArrayObject([
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary'
                                    ]
                                ]
                            ]
                        ]
                    ])
                )
            )
        )
    ]
)]
#[ApiFilter(
    SimpleSearchFilter::class,
    properties: ['name', 'description', 'tags.name'],
    arguments: ['searchParameterName' => 's'],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'partial',
    'description' => 'partial',
    'tags.name' => 'partial',
    'tags' => 'exact',
    'owner.pseudo' => 'partial',
    'owner' => 'exact',
    'borrower.pseudo' => 'partial',
    'borrower' => 'exact',
])]
#[ApiFilter(OrderFilter::class, properties: [
    'owner.city', 'name', 'tags.name'
])]
class Item
{
    use HasTimestampTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 128)]
    #[Groups([
        'item:item:get',
        'item:collection:get',
        'item:write'
    ])]
    #[Assert\NotNull]
    #[Assert\NotBlank]
    private string $name = '';

    #[ORM\Column(length: 128, unique: true)]
    #[Slug(fields: ['name'])]
    #[ApiProperty(identifier: true)]
    #[Groups([
        'item:item:get',
        'item:collection:get',
        'item:write',
        'tag:item:'
    ])]
    private ?string $slug = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups([
        'item:write',
        'item:item:get',
        'item:collection:get',
        'user:item:get',
        'tag:item:get',
    ])]
    private ?string $description = null;

    #[ORM\OneToOne(inversedBy: 'item', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Assert\NotNull(groups: ['image_create'])]
    private ?Image $image = null;

    #[SerializedName('image')]
    #[Groups(['item:item:get', 'item:collection:get'])]
    public function getImageUrl(): ?string
    {
        return $this->image?->contentUrl;
    }

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'items', cascade: ['persist'])]
    #[Groups(['item:item:get', 'item:collection:get', 'item:write'])]
    private Collection $tags;

    #[ORM\ManyToOne(inversedBy: 'properties')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['item:item:get', 'item:collection:get', 'item:write'])]
    #[Assert\NotNull]
    private ?User $owner = null;

    #[ORM\ManyToOne(inversedBy: 'loans')]
    #[Groups(['item:item:get', 'item:write'])]
    private ?User $borrower = null;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getBorrower(): ?User
    {
        return $this->borrower;
    }

    public function setBorrower(?User $borrower): self
    {
        $this->borrower = $borrower;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
