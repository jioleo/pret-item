<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\OpenApi\Model;
use App\Controller\CreateImageAction;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Slug;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    denormalizationContext: ['groups' => ['user:write']],
    operations: [
        new Get(
            normalizationContext: ['groups' => ['user:item:get']],
            security: "is_granted('USER_VIEW', object)",
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['user:collection:get']],
        ),
        new Post(
            validationContext: ['groups' => ['Default', 'postValidation']],
        ),
        new Patch(
            security: "is_granted('USER_EDIT', object)"
        ),
        new Delete(security: "is_granted('USER_DELETE', object)"),
    ],
)]
#[ApiResource(
    uriTemplate: 'users/{slug}/image',
    operations: [
        new Post(
            security: "is_granted('EDIT', object)",
            controller: CreateImageAction::class,
            deserialize: false,
            validationContext: ['groups' => ['Default', 'image_create']],
            openapi: new Model\Operation(
                requestBody: new Model\RequestBody(
                    content: new \ArrayObject([
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary'
                                    ]
                                ]
                            ]
                        ]
                    ])
                )
            )
        )
    ]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Serializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(['user:write', 'user:secure', 'user:item:get'])]
    #[Assert\Email]
    #[Assert\NotBlank]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Groups([
        'user:write',
        'user:collection:get',
        'user:item:get',
        'item:collection:get',
        'item:item:get',
    ])]
    #[Assert\NotBlank]
    private string $pseudo = '';

    #[ORM\Column(length: 128, unique: true)]
    #[Slug(fields: ['pseudo'])]
    #[ApiProperty(identifier: true)]
    #[Groups([
        'user:write',
        'user:collection:get',
        'user:item:get',
        'item:item:get',
        'item:collection:get',
    ])]
    private ?string $slug = null;

    /**
     * @var array<string>
     */
    #[ORM\Column]
    #[Groups(['user:secure', 'user:item:get'])]
    private array $roles = [];

    #[Groups(['user:write'])]
    #[SerializedName('password')]
    #[Assert\NotBlank(groups: ['postValidation'])]
    #[Assert\Length(min: 4, groups: ['postValidation'])]
    #[SecurityAssert\UserPassword(groups: ['postValidation'])]
    // #[Assert\NotCompromisedPassword]
    // #[Assert\PasswordStrength(groups: ['postValidation'])]
    private ?string $plainPassword = null;

    #[ORM\Column]
    private ?string $password = null;

    #[ORM\OneToOne(inversedBy: 'userLinked', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Assert\NotNull(groups: ['image_create'])]
    private ?Image $avatar = null;

    #[Groups([
        'user:collection:get',
        'user:item:get',
        'item:collection:get',
        'item:item:get'
    ])]
    #[SerializedName('avatar')]
    public function getAvatarUrl(): ?string
    {
        return $this->avatar?->contentUrl;
    }

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([
        'user:write',
        'user:item:get',
        'user:collection:get',
        'item:item:get',
        'item:collection:get'
    ])]
    private ?string $city = null;

    /**
     * @var Collection<int, Item>
     */
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Item::class, orphanRemoval: true)]
    private Collection $properties;

    /**
     * @var Collection<int, Item>
     */
    #[ORM\OneToMany(mappedBy: 'borrower', targetEntity: Item::class)]
    private Collection $loans;

    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->loans = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Summary of setRoles.
     *
     * @param array<string> $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(Item $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties->add($property);
            $property->setOwner($this);
        }

        return $this;
    }

    public function removeProperty(Item $property): self
    {
        if ($this->properties->removeElement($property)) {
            // set the owning side to null (unless already changed)
            if ($property->getOwner() === $this) {
                $property->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getLoans(): Collection
    {
        return $this->loans;
    }

    public function addLoan(Item $loan): self
    {
        if (!$this->loans->contains($loan)) {
            $this->loans->add($loan);
            $loan->setBorrower($this);
        }

        return $this;
    }

    public function removeLoan(Item $loan): self
    {
        if ($this->loans->removeElement($loan)) {
            // set the owning side to null (unless already changed)
            if ($loan->getBorrower() === $this) {
                $loan->setBorrower(null);
            }
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function __toString(): string
    {
        return $this->pseudo;
    }

    public function getAvatar(): ?Image
    {
        return $this->avatar;
    }

    public function getImage(): ?Image
    {
        return $this->avatar;
    }

    public function setAvatar(?Image $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function serialize()
    {
        return serialize([
            $this->password,
            $this->id,
            $this->email,
            $this->roles,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->password,
            $this->id,
            $this->email,
            $this->roles,
        ) = unserialize($serialized);
    }
}
