<?php
// api/src/Controller/CreateimageAction.php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Item;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
final class CreateImageAction extends AbstractController
{
    public function __invoke(Request $request): Item|User
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $image = new Image();
        $image->setFile($uploadedFile);

        $obj = $request->attributes->get('data');

        if ($obj instanceof Item) {
            $obj->setImage($image);
        } else if ($obj instanceof User) {
            $obj->setAvatar($image);
        } else {
            throw new BadRequestHttpException('No instance to link image to');
        }

        return $obj;
    }
}