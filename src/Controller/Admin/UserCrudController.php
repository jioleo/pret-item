<?php

namespace App\Controller\Admin;

use App\EasyAdmin\Field\VichImageField;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AvatarField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserCrudController extends AbstractCrudController
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.users')
            ->setPageTitle(Crud::PAGE_DETAIL, fn (?User $user) => $user?->getPseudo())
            ->setPageTitle(Crud::PAGE_EDIT, fn (?User $user) => $this->translator->trans('edit.title', ['name' => $user?->getPseudo()]));
    }

    public function configureFields(string $pageName): iterable
    {
        // INFOS
        yield $panelInfo = FormField::addPanel('crud.panel.user.info')
            ->setIcon('fa fa-info')
        ;
        yield IdField::new('id')->hideOnForm()
        ;
        yield EmailField::new('email', 'crud.fields.email')
            ->setColumns(6)
        ;
        yield TextField::new('pseudo', 'crud.fields.pseudo')
            ->setColumns(6)
        ;
        yield TextField::new('plainPassword', 'crud.fields.password')->onlyOnForms()
            ->setColumns(6)
        ;
        yield TextField::new('city', 'crud.fields.city')->onlyOnForms()
            ->setColumns(6)
        ;
        yield ChoiceField::new('roles', 'crud.fields.roles')
            ->setChoices([
                'Super admin' => 'ROLE_SUPER_ADMIN',
                'Administrateur' => 'ROLE_ADMIN',
            ])
            ->allowMultipleChoices()
            ->renderExpanded()
            ->renderAsBadges()
            ->setRequired(false)
            ->setColumns(6)
        ;
        // AVATAR
        yield $panelAvatar = FormField::addPanel();
        yield VichImageField::new('avatar', 'crud.fields.avatar');

        // OBJECTS
        yield $panelObjects = FormField::addPanel('crud.panel.user.objects')->setIcon('fa fa-list');

        $association = function ($property, $label) {
            return AssociationField::new($property, $label)
            ->hideOnDetail()
            ->autocomplete()
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->setColumns(6);
        };
        yield $association('properties', 'crud.fields.properties');
        yield $association('loans', 'crud.fields.loans');

        yield CollectionField::new('properties', 'crud.fields.properties')->onlyOnDetail()
            ->setTemplatePath('admin/fields/user_objects_list.html.twig')
            // use in the template to no the current property
            ->setHelp('borrower')
            ->setColumns(6);
        yield CollectionField::new('loans', 'crud.fields.loans')->onlyOnDetail()
            ->setTemplatePath('admin/fields/user_objects_list.html.twig')
            // use in the template to no the current property
            ->setHelp('owner')
            ->setColumns(6);

        if ($pageName === Crud::PAGE_EDIT || $pageName === Crud::PAGE_NEW) {
            $panelObjects->addCssClass('col-12 order-3');
            $panelAvatar->addCssClass('col-2 order-1');
            $panelInfo->addCssClass('col-10 order-2');
        }
    }
}
