<?php

namespace App\Controller\Admin;

use App\Entity\Friendship;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class FriendshipCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Friendship::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('requester'),
            AssociationField::new('receiver'),
            ChoiceField::new('status')->setChoices(array_combine(Friendship::STATUS, Friendship::STATUS))
        ];
    }

}
