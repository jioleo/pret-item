<?php

namespace App\Controller\Admin;

use App\Entity\Friendship;
use App\Entity\Item;
use App\Entity\Tag;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private UploaderHelper $uploaderHelper, private TranslatorInterface $translator)
    {
    }

    #[Route(path: '/admin', name: 'admin_dashboard_index')]
    public function index(): Response
    {
        // On affiche un dashboard pour l'instant vide.
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle($this->translator->trans('app.name'))
            // ->renderContentMaximized()
        ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        assert($user instanceof User);

        return parent::configureUserMenu($user)
            ->setAvatarUrl($this->uploaderHelper->asset($user->getAvatar(), 'file'))
            ->addMenuItems([
                MenuItem::linkToUrl('menu.user.profile', 'fas fa-user', '#'),
            ]);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('menu.main.dashboard', 'fa fa-dashboard');

        yield MenuItem::section('menu.main.section.main_data');
        yield MenuItem::linkToCrud('entity.items', 'fas fa-poo', Item::class);
        yield MenuItem::linkToCrud('entity.tags', 'fas fa-tags', Tag::class);

        yield MenuItem::section('menu.main.section.users');
        yield MenuItem::linkToCrud('entity.users', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('entity.friendship', 'fas fa-user-group', Friendship::class);
    }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            // ->renderContentMaximized()
            ->showEntityActionsInlined()
            // ->setDefaultSort([
            //     'id' => 'DESC',
            // ])
        ;
    }

    public function configureActions(): Actions
    {
        $btnAction = function (Action $action) {
            return $action
                ->addCssClass('btn');
        };
        $deleteAction = function (Action $action) use ($btnAction) {
            return $btnAction($action)->setIcon('fa fa-trash-o');
        };
        $editAction = function (Action $action) use ($btnAction) {
            return $btnAction($action)->setIcon('fa fa-pen');
        };

        $detailAction = function (Action $action) use ($btnAction) {
            return $btnAction($action)->setIcon('fa fa-eye');
        };

        $newAction = function (Action $action) use ($btnAction) {
            return $btnAction($action)->setIcon('fa fa-add');
        };

        $indexAction = function (Action $action) use ($btnAction) {
            return $btnAction($action)->setIcon('fa fa-list');
        };

        $editAction = function (Action $action) use ($btnAction) {
            return $btnAction($action)->setIcon('fa fa-edit');
        };

        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_INDEX, Action::DELETE, fn (Action $action) => $deleteAction($action)->setLabel(false))
            ->update(Crud::PAGE_INDEX, Action::EDIT, fn (Action $action) => $editAction($action)->setLabel(false))
            ->update(Crud::PAGE_INDEX, Action::DETAIL, fn (Action $action) => $detailAction($action)->setLabel(false))
            ->update(Crud::PAGE_INDEX, Action::NEW, $newAction)
            ->update(Crud::PAGE_DETAIL, Action::DELETE, $deleteAction)
            ->update(Crud::PAGE_DETAIL, Action::EDIT, $editAction)
            ->update(Crud::PAGE_DETAIL, Action::INDEX, $indexAction)
            ->update(Crud::PAGE_EDIT, Action::INDEX, $indexAction)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, $editAction)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, $editAction)
            ->reorder(Crud::PAGE_DETAIL, [
                Action::EDIT,
                Action::INDEX,
                Action::DELETE,
            ]);
    }

    public function configureAssets(): Assets
    {
        return parent::configureAssets()
            ->addWebpackEncoreEntry('admin');
    }

}
