<?php

namespace App\Controller\Admin;

use App\Entity\Item;
use App\EasyAdmin\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Symfony\Contracts\Translation\TranslatorInterface;

class ItemCrudController extends AbstractCrudController
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public static function getEntityFqcn(): string
    {
        return Item::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.items')
            ->setPageTitle(Crud::PAGE_DETAIL, fn (?Item $item) => $item?->getName())
            ->setPageTitle(Crud::PAGE_EDIT, fn (?Item $item) => $this->translator->trans('edit.title', ['name' => $item?->getName()]));
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addPanel()->addCssClass('col-6');

        yield IdField::new('id')->hideOnForm();

        yield TextField::new('name', 'crud.fields.name')
            ->setColumns(12);

        yield TextField::new('slug', 'slug')
        ->setColumns(12)->hideOnForm();

        yield TextEditorField::new('description', 'crud.fields.description')
            ->setColumns(12);

        yield AssociationField::new('tags', 'crud.fields.tags')
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
            ->setTemplatePath('admin/fields/tag_children.html.twig')
            ->setColumns(12);

        yield FormField::addPanel()->addCssClass('col-6');
        yield VichImageField::new('image', 'crud.fields.image');

        yield FormField::addPanel('crud.panel.item.onwer-borrower');

        yield AssociationField::new('owner', 'crud.fields.owner')
            ->autocomplete()
            ->setColumns(6);
        yield AssociationField::new('borrower', 'crud.fields.borrower')
            ->autocomplete()
            ->setColumns(6);

        yield DateField::new('createdAt', 'crud.fields.createdAt')->setDisabled();
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('tags', 'crud.fields.tags'))
            ->add(EntityFilter::new('owner', 'crud.fields.owner'))
            ->add(EntityFilter::new('borrower', 'crud.fields.borrower'))
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions);
    }
}
