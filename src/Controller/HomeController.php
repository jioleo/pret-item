<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'index')]
    #[Route(
        '/{vueRouting}',
        name: 'index_vue_routing',
        requirements: ['vueRouting' => '.*'],
        condition: "(params['vueRouting'] contains 'admin') === false and (params['vueRouting'] contains 'api/') === false and context.getMethod() in ['GET', 'HEAD']"
    )]
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }
}
